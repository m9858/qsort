#include <iostream>
#include <fstream>
#include <time.h>

#include "graf.h"

std::vector<int> openFile(const std::string& file) {
	std::ifstream fin (file);
	if ( !fin.is_open() ) {
		throw std::invalid_argument("Error: not file!!");
	}
	std::vector<int> arr;
	int a;
	while (fin >> a ){
		arr.push_back(a);
	}
	return arr;
}


void qsort(std::vector<int>& arr, int begin, int end, int pX, int pY, const char* Sob, bool avto) {
	if ( !(begin < end) ) {
		return;
	}
	int left = begin;
	int right = end;
	int middle = (left + right) / 2;
	int step = 0;
	bool stop = false, swap = false, END_input = true, reck = false, run = false;
        clock_t ben = clock();
	while ( !WindowShouldClose() )  {
		if (!avto && IsKeyPressed(KEY_ENTER) ) {
			run = true;
		}
		if ( avto && static_cast<double>( clock() - ben ) / CLOCKS_PER_SEC >= 0.3 ) {
			run = true;
			ben = clock();
		}
		if ( run && !END_input ) {
			if ( step == 0 ) {
				step = 1;
				while (arr[left] < arr[middle]) {
					left++;
					step = 0;
					break;
				}
			}
			if ( step == 1 ) {
				step = 3;
				while ( arr[right] > arr[middle] ) {
					right--;
					step = 1; 
					break;
				}
				if ( (left <= right) && step == 3 ) {
					step = 2;
					swap = true;
					stop = true;
				}
			}
			if ( step == 2 && !stop ) {
				std::swap(arr[left], arr[right]);
				left++;
				right--;
				step = 3;
				stop = true;
				swap = false;
			}
			if ( step == 3 ) {
				if ( !(begin < right) ) {
					reck = true;
				}
				if  ( !( left < end) ) {
					step = 4;
				}
			}
			if ( step == 3 && !stop) {
				std::string rastR = "Right distance line: " + std::to_string(left + 1) + "-" + std::to_string(end + 1);
				std::string rastL = "Left distance line: " + std::to_string(begin + 1) + "-" + std::to_string(right + 1);
				if ( !reck ) {
	       				qsort(arr, begin, right, pX, pY + 80, rastL.c_str(), avto);
					reck = true;
				}
				else {	
	       				qsort(arr, left, end, pX, pY + 80, rastR.c_str(), avto);
					step = 4;
				}
				stop = true;
				ben = clock();
			}
			if ( step == 4 && !stop) {
				return ;
			}

		}
		
			
		BeginDrawing(); 
		{
			clearX(pX - 10, pY, 1600, 500);
       	    		DrawText("Qsort", 1600 / 2 - 20 * 3 , 10, 20, LIGHTGRAY);
			DrawText(Sob, pX, pY, 20, LIGHTGRAY);
			size_t x1 = printData(arr, begin, end + 1 , left, right, middle, pX, pY + 22, swap);
			if ( step == 4 ) {
				const char* other = "The recursive operation has come to an end, press enter to go up a level?";
				DrawText(other,  x1 + 10, pY + 22, 20, LIGHTGRAY);
			}
			if ( step == 3 ) {
				const char* other = reck ?  "To go to the right array, press enter?" : "To go to the left array, press enter?";
				DrawText(other,  x1 + 10, pY + 22, 20, LIGHTGRAY);
			}
			designations();
		}
		EndDrawing();
		
		stop = false;
		run = false; 
		END_input = false;
		}
}


int main(int argc, char** argv) {
	if ( argc != 3 ) {
		std::cerr << "Error: errors in arguments!!" << std::endl;
		return 1;
	}
	std::vector<int> data;
	try {
		data = openFile(argv[2]);
	}
	catch ( std::invalid_argument& error) {
		std::cerr << error.what() << std::endl;
		return 1;
	}
	bool awto;
	std::string s (argv[1]);
	if ( s == "auto") {
		awto = true;
	}
	else if ( s == "manual") {
		awto = false;
	}
	else {
		std::cerr << "Error: there is no type of code launch ( it should be auto or manual)!!" << std::endl;
		return -1;
	}
	const int X = 1600;
	const int Y = 1000;
	int pX = 10, pY = 10 + 22;
    	InitWindow(X, Y, "Qsort");
	std::string sob = "Press the enter button to start sorting the array";
	bool end = true, swap = false;
	while ( !WindowShouldClose() ) {
		if ( IsKeyPressed(KEY_ENTER) && end ) {
			qsort(data, 0, data.size() - 1 , 10, 10, "Main line", awto);
			sob = "The array is sorted press enter to exit!!!";
			end = false;
		}
		else if ( IsKeyPressed(KEY_ENTER) ) {
			break;
		}
		BeginDrawing();
		{
		        ClearBackground(BLACK);
       	    		DrawText("Qsort", 1600 / 2 - 20 * 3 , 10, 20, LIGHTGRAY);
			DrawText(sob.c_str(), 10, 10, 20, LIGHTGRAY);
			printData(data, 0, data.size() , -1, -1, -1, pX, pY, swap);
			designations();
		}
		EndDrawing();
	}
        CloseWindow();

        return 0;
}
