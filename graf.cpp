#include "graf.h"

void clearX(int x, int y, int x1, int y1) {
	DrawRectangle(x, y, x1, y1, BLACK);
}


void designations() {
	DrawRectangle(10, 960, 20, 20, RED);
	DrawText("-support element (in most cases it is the middle one)!!!", 31, 960, 20, LIGHTGRAY);
	DrawRectangle(10, 920, 20, 20, BLUE);
	DrawText("-the passed element it can change places will be recorded from case to case)!!!", 31, 920, 20, LIGHTGRAY);
	DrawLineEx( {20, 880}, {20, 900}, 3, ORANGE); 
	DrawText("-flag on what we are swapping with what!!!", 31, 880, 20, LIGHTGRAY);
	DrawLineEx( {20, 840}, {20, 860}, 3, PINK); 
	DrawText("-left pointer to values", 31, 840, 20, LIGHTGRAY);
	DrawLineEx( {20, 800}, {20, 820}, 3, GREEN); 
	DrawText("-left pointer to values", 31, 800, 20, LIGHTGRAY);	
}


size_t printData(std::vector<int>& arr, int i1, int size, int l, int r, int m, int x, int y, bool swap){
	Vector2 swap1;
	Vector2 swap2;
	for (int i = 0; i < arr.size(); i++) {
		if ( i < l || i > r ) {
			DrawRectangle(x-5, y, 10 * std::to_string(arr[i]).size() + 13, 20, BLUE);
		}
			
		if ( i == m ) {
			DrawRectangle(x - 5, y, 10 * std::to_string(arr[i]).size() + 13, 20, RED);
		}
		if ( i == l ) {
			int tmp1 = y + 20;
			int tmp2 = y + 40;
			Vector2 loc1 = {x + 0.f, tmp1 + 0.f};
			Vector2 loc2 = {x + 0.f, tmp2 + 0.f};
			if ( swap ) {
				swap2 = loc2;
				DrawLineEx(loc1, loc2, 3, ORANGE);
			}
			else {
	                        DrawLineEx(loc1, loc2, 3, PINK);
			}
		}
		if ( i == r) {
			int tmpX = x + 10 * std::to_string(arr[i]).size();
			int tmpY = y + 20;
			Vector2 loc1 = {tmpX + 0.f, tmpY + 0.f};
			tmpY += 20;
			Vector2 loc2 = {tmpX + 0.f, tmpY + 0.f};
			if ( swap ) {
				swap1 = loc2;
				DrawLineEx(loc1, loc2, 3,  ORANGE);
			}
			else {
	                        DrawLineEx(loc1, loc2, 3, GREEN);
			}
		}
		DrawText(std::to_string(arr[i]).c_str(), x, y, 20, LIGHTGRAY);
		x += 10 * std::to_string(arr[i]).size() + 25;
	}
	if ( swap ) {
		DrawLineEx(swap1, swap2, 3, ORANGE);
	}
	return x;
}
