.PHONY: all, clean


all:main


main:main.o graf.o
	c++ -lraylib  $^ -o qsort



%.o: %.c
	c++ -lraylib -c $< -o $*.o

clean:
	rm -f *.o qsort

input.txt:main
	./qsort auto input.txt > /dev/null 



